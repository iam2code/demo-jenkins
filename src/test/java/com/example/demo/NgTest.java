package com.example.demo;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@SpringBootTest(classes = {DemoApplication.class})
public class NgTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private HelloService helloService;

    static org.slf4j.Logger log = LoggerFactory.getLogger(DemoApplicationTests.class);

    @BeforeClass
    public void beforeClass() {
        log.info("Before class");
    }

    @DataProvider(name = "test1")
    public Object[][] createData1() {
        return new Object[][] {
                { "Cedric", new Integer(36) },
                { "Anne", new Integer(37)},
        };
    }

    @DataProvider(name = "test2")
    public Object[][] createData2() {
        return new Object[][] {
                { "Cedric", new Integer(36) },
                { "Anne", new Integer(37)},
        };
    }



    @Test(dataProvider = "test1")
    public void test1(String name, Integer edad) {
        log.info("Name: " + name + ", edad: " + edad);
    }

    @Test
    public void testHola() {
        log.info(helloService.hola());

    }

}
